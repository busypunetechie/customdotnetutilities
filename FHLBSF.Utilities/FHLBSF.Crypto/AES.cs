﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Security;
using System.Security.Cryptography;
using System.Security.Principal;

namespace FHLBSF.Crypto
{
    /// <summary>
    /// Facilitates Advanced Encryption Standard (Rijndael AES-256) implementation for 
    /// simple encryption and decryption of Connection Strings, and such...
    /// </summary>
    public class AES
    {
        private byte[] _key;
        private byte[] _iv;
        private bool _error;
        private string _lastError;

        /// <summary>
        /// Encryption/Decryption Key
        /// </summary>
        public byte[] Key
        {
            get
            {
                return _key;
            }
            set
            {
                _key = value;
            }
        }
        /// <summary>
        /// Initialization Vector
        /// </summary>
        public byte[] IV
        {
            get
            {
                return _iv;
            }
            set
            {
                _iv = value;
            }
        }
        /// <summary>
        /// True ==> an error occured during the last operation
        /// </summary>
        public bool Error
        {
            get
            {
                return _error;
            }
            set
            {
                _error = value;
            }
        }
        /// <summary>
        /// Error message if an error occured during the last operation
        /// </summary>
        public string LastError
        {
            get
            {
                return _lastError;
            }
            set
            {
                _lastError = value;
            }
        }
        /// <summary>
        /// Base-64 encoded Key
        /// </summary>
        public string KeyEncoded
        {
            get
            {
                return Base64.Encode(_key);
            }
            set
            {
                _key = Base64.Decode(value);
            }
        }
        /// <summary>
        /// Base-64 encoded IV
        /// </summary>
        public string IVEncoded
        {
            get
            {
                return Base64.Encode(_iv);
            }
            set
            {
                _iv = Base64.Decode(value);
            }
        }
        /// <summary>
        /// Create a new instance with a provided Key and Initialization Vector
        /// </summary>
        /// <param name="key">Encryption/Decryption Key</param>
        /// <param name="iv">Initialization Vector</param>
        public AES(byte[] key, byte[] iv)
        {
            _key = key;
            _iv = iv;

            _error = false;
            _lastError = string.Empty;
        }
        /// <summary>
        /// Create a new instance with a randomly generated Key and Initialization Vector
        /// </summary>
        public AES()
        {
            RijndaelManaged aes = new RijndaelManaged();
            aes.GenerateKey();
            aes.GenerateIV();
            _key = new byte[aes.Key.Length];
            _iv = new byte[aes.IV.Length];
            aes.Key.CopyTo(_key, 0);
            aes.IV.CopyTo(_iv, 0);
            aes.Clear();

            _error = false;
            _lastError = string.Empty;
        }
        /// <summary>
        /// Encrypt a plain text string into cipher text
        /// </summary>
        /// <param name="plainText">Input Text</param>
        /// <returns>bytes of encrypted data</returns>
        public byte[] Encrypt(string plainText)
        {
            _error = false;
            _lastError = string.Empty;

            MemoryStream ms = null;
            RijndaelManaged aes = null;

            try
            {
                aes = new RijndaelManaged();
                aes.Key = _key;
                aes.IV = _iv;

                ICryptoTransform enc = aes.CreateEncryptor(aes.Key, aes.IV);
                ms = new MemoryStream();

                using (CryptoStream cs = new CryptoStream(ms, enc, CryptoStreamMode.Write))
                {
                    using (StreamWriter sw = new StreamWriter(cs))
                    {
                        sw.Write(plainText);
                    }
                }

                return ms.ToArray();
            }
            catch (Exception ex)
            {
                if (aes != null)
                    aes.Clear();

                _error = true;
                _lastError = string.Format("{0} {1}", ex.Message + (ex.InnerException != null ? ex.InnerException.Message : string.Empty));

                return null;
            }
        }
        /// <summary>
        /// Decrypt a sequence of bytes into plain text
        /// </summary>
        /// <param name="cipherText">Cipher Text</param>
        /// <returns>original (decrypted) text</returns>
        public string Decrypt(byte[] cipherText)
        {
            _error = false;
            _lastError = string.Empty;
            string plainText = null;

            RijndaelManaged aes = null;

            try
            {
                aes = new RijndaelManaged();
                aes.Key = _key;
                aes.IV = _iv;

                ICryptoTransform dec = aes.CreateDecryptor(aes.Key, aes.IV);
                using (MemoryStream ms = new MemoryStream(cipherText))
                {
                    using (CryptoStream cs = new CryptoStream(ms, dec, CryptoStreamMode.Read))
                    {
                        using (StreamReader sr = new StreamReader(cs))
                        {
                            plainText = sr.ReadToEnd();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                if (aes != null)
                    aes.Clear();

                _error = true;
                _lastError = string.Format("{0} {1}", ex.Message, (ex.InnerException != null ? ex.InnerException.Message : string.Empty));
            }

            return plainText;
        }
        /// <summary>
        /// Encrypt and Base-64 Encode plain text
        /// </summary>
        /// <param name="plainText">Input Plain Text</param>
        /// <returns>Encrypted and Base-64 encoded data</returns>
        public string EncryptAndEncode(string plainText)
        {
            return Base64.Encode(this.Encrypt(plainText));
        }
        /// <summary>
        /// base-64 Decode and Decrypt cipher text
        /// </summary>
        /// <param name="cipherText">Input Base-64 encoded Cipher Text</param>
        /// <returns>Original Plain Text</returns>
        public string DecodeAndDecrypt(string cipherText)
        {
            return this.Decrypt(Base64.Decode(cipherText));
        }

    }
}
