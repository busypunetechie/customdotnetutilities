﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;

namespace FHLBSF.Crypto
{
    public class Base64
    {
        public static string Encode(byte[] input)
        {
            return Convert.ToBase64String(input, Base64FormattingOptions.None);
        }
        public static byte[] Decode(string input)
        {
            return Convert.FromBase64String(input);
        }
    }
}
