﻿namespace FHLBSF.EncUtil
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtPlainText = new System.Windows.Forms.TextBox();
            this.btnEncrypt = new System.Windows.Forms.Button();
            this.btnDecrypt = new System.Windows.Forms.Button();
            this.txtCipherText = new System.Windows.Forms.TextBox();
            this.txtKeyEncoded = new System.Windows.Forms.TextBox();
            this.txtIVEncoded = new System.Windows.Forms.TextBox();
            this.gbxEncrypt = new System.Windows.Forms.GroupBox();
            this.txtEncryptedText = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btnReset = new System.Windows.Forms.Button();
            this.gbxDecrypt = new System.Windows.Forms.GroupBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtIVEncoded2 = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtKeyEncoded2 = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.txtDecryptedText = new System.Windows.Forms.TextBox();
            this.chkGenerateKeyAndIV = new System.Windows.Forms.CheckBox();
            this.gbxEncrypt.SuspendLayout();
            this.gbxDecrypt.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtPlainText
            // 
            this.txtPlainText.Location = new System.Drawing.Point(97, 28);
            this.txtPlainText.Name = "txtPlainText";
            this.txtPlainText.Size = new System.Drawing.Size(284, 20);
            this.txtPlainText.TabIndex = 0;
            // 
            // btnEncrypt
            // 
            this.btnEncrypt.Location = new System.Drawing.Point(97, 54);
            this.btnEncrypt.Name = "btnEncrypt";
            this.btnEncrypt.Size = new System.Drawing.Size(75, 23);
            this.btnEncrypt.TabIndex = 1;
            this.btnEncrypt.Text = "Encrypt";
            this.btnEncrypt.UseVisualStyleBackColor = true;
            this.btnEncrypt.Click += new System.EventHandler(this.btnEncrypt_Click);
            // 
            // btnDecrypt
            // 
            this.btnDecrypt.Location = new System.Drawing.Point(97, 149);
            this.btnDecrypt.Name = "btnDecrypt";
            this.btnDecrypt.Size = new System.Drawing.Size(75, 23);
            this.btnDecrypt.TabIndex = 2;
            this.btnDecrypt.Text = "Decrypt";
            this.btnDecrypt.UseVisualStyleBackColor = true;
            this.btnDecrypt.Click += new System.EventHandler(this.btnDecrypt_Click);
            // 
            // txtCipherText
            // 
            this.txtCipherText.Location = new System.Drawing.Point(97, 28);
            this.txtCipherText.Name = "txtCipherText";
            this.txtCipherText.Size = new System.Drawing.Size(284, 20);
            this.txtCipherText.TabIndex = 3;
            // 
            // txtKeyEncoded
            // 
            this.txtKeyEncoded.Location = new System.Drawing.Point(97, 97);
            this.txtKeyEncoded.Name = "txtKeyEncoded";
            this.txtKeyEncoded.ReadOnly = true;
            this.txtKeyEncoded.Size = new System.Drawing.Size(284, 20);
            this.txtKeyEncoded.TabIndex = 4;
            // 
            // txtIVEncoded
            // 
            this.txtIVEncoded.Location = new System.Drawing.Point(97, 123);
            this.txtIVEncoded.Name = "txtIVEncoded";
            this.txtIVEncoded.ReadOnly = true;
            this.txtIVEncoded.Size = new System.Drawing.Size(284, 20);
            this.txtIVEncoded.TabIndex = 5;
            // 
            // gbxEncrypt
            // 
            this.gbxEncrypt.Controls.Add(this.chkGenerateKeyAndIV);
            this.gbxEncrypt.Controls.Add(this.txtEncryptedText);
            this.gbxEncrypt.Controls.Add(this.label4);
            this.gbxEncrypt.Controls.Add(this.label3);
            this.gbxEncrypt.Controls.Add(this.label2);
            this.gbxEncrypt.Controls.Add(this.label1);
            this.gbxEncrypt.Controls.Add(this.btnReset);
            this.gbxEncrypt.Controls.Add(this.txtPlainText);
            this.gbxEncrypt.Controls.Add(this.txtKeyEncoded);
            this.gbxEncrypt.Controls.Add(this.txtIVEncoded);
            this.gbxEncrypt.Controls.Add(this.btnEncrypt);
            this.gbxEncrypt.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbxEncrypt.Location = new System.Drawing.Point(26, 39);
            this.gbxEncrypt.Name = "gbxEncrypt";
            this.gbxEncrypt.Size = new System.Drawing.Size(397, 328);
            this.gbxEncrypt.TabIndex = 6;
            this.gbxEncrypt.TabStop = false;
            this.gbxEncrypt.Text = "Encrypt";
            // 
            // txtEncryptedText
            // 
            this.txtEncryptedText.Location = new System.Drawing.Point(97, 194);
            this.txtEncryptedText.Multiline = true;
            this.txtEncryptedText.Name = "txtEncryptedText";
            this.txtEncryptedText.Size = new System.Drawing.Size(284, 107);
            this.txtEncryptedText.TabIndex = 14;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 197);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(81, 13);
            this.label4.TabIndex = 12;
            this.label4.Text = "Encypted Data:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 126);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(73, 13);
            this.label3.TabIndex = 11;
            this.label3.Text = "Encryption IV:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 100);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(81, 13);
            this.label2.TabIndex = 10;
            this.label2.Text = "Encryption Key:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(82, 13);
            this.label1.TabIndex = 9;
            this.label1.Text = "Text to Encrypt:";
            // 
            // btnReset
            // 
            this.btnReset.Location = new System.Drawing.Point(273, 149);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(108, 23);
            this.btnReset.TabIndex = 8;
            this.btnReset.Text = "Reset Key and IV";
            this.btnReset.UseVisualStyleBackColor = true;
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // gbxDecrypt
            // 
            this.gbxDecrypt.Controls.Add(this.label5);
            this.gbxDecrypt.Controls.Add(this.txtIVEncoded2);
            this.gbxDecrypt.Controls.Add(this.label6);
            this.gbxDecrypt.Controls.Add(this.txtKeyEncoded2);
            this.gbxDecrypt.Controls.Add(this.label7);
            this.gbxDecrypt.Controls.Add(this.label8);
            this.gbxDecrypt.Controls.Add(this.txtDecryptedText);
            this.gbxDecrypt.Controls.Add(this.txtCipherText);
            this.gbxDecrypt.Controls.Add(this.btnDecrypt);
            this.gbxDecrypt.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbxDecrypt.Location = new System.Drawing.Point(446, 39);
            this.gbxDecrypt.Name = "gbxDecrypt";
            this.gbxDecrypt.Size = new System.Drawing.Size(397, 328);
            this.gbxDecrypt.TabIndex = 7;
            this.gbxDecrypt.TabStop = false;
            this.gbxDecrypt.Text = "Decrypt";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 197);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(85, 13);
            this.label5.TabIndex = 16;
            this.label5.Text = "Decrypted Data:";
            // 
            // txtIVEncoded2
            // 
            this.txtIVEncoded2.Location = new System.Drawing.Point(97, 123);
            this.txtIVEncoded2.Name = "txtIVEncoded2";
            this.txtIVEncoded2.Size = new System.Drawing.Size(284, 20);
            this.txtIVEncoded2.TabIndex = 8;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 126);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(74, 13);
            this.label6.TabIndex = 15;
            this.label6.Text = "Decryption IV:";
            // 
            // txtKeyEncoded2
            // 
            this.txtKeyEncoded2.Location = new System.Drawing.Point(97, 97);
            this.txtKeyEncoded2.Name = "txtKeyEncoded2";
            this.txtKeyEncoded2.Size = new System.Drawing.Size(284, 20);
            this.txtKeyEncoded2.TabIndex = 8;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 100);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(82, 13);
            this.label7.TabIndex = 14;
            this.label7.Text = "Decryption Key:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 31);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(83, 13);
            this.label8.TabIndex = 13;
            this.label8.Text = "Text to Decrypt:";
            // 
            // txtDecryptedText
            // 
            this.txtDecryptedText.Location = new System.Drawing.Point(97, 194);
            this.txtDecryptedText.Name = "txtDecryptedText";
            this.txtDecryptedText.Size = new System.Drawing.Size(284, 20);
            this.txtDecryptedText.TabIndex = 6;
            // 
            // chkGenerateKeyAndIV
            // 
            this.chkGenerateKeyAndIV.AutoSize = true;
            this.chkGenerateKeyAndIV.Location = new System.Drawing.Point(97, 153);
            this.chkGenerateKeyAndIV.Name = "chkGenerateKeyAndIV";
            this.chkGenerateKeyAndIV.Size = new System.Drawing.Size(121, 17);
            this.chkGenerateKeyAndIV.TabIndex = 15;
            this.chkGenerateKeyAndIV.Text = "Override Key and IV";
            this.chkGenerateKeyAndIV.UseVisualStyleBackColor = true;
            this.chkGenerateKeyAndIV.CheckedChanged += new System.EventHandler(this.chkGenerateKeyAndIV_CheckedChanged);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(869, 396);
            this.Controls.Add(this.gbxDecrypt);
            this.Controls.Add(this.gbxEncrypt);
            this.Name = "Form1";
            this.Text = "FHLBSF Encryption Utility";
            this.gbxEncrypt.ResumeLayout(false);
            this.gbxEncrypt.PerformLayout();
            this.gbxDecrypt.ResumeLayout(false);
            this.gbxDecrypt.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox txtPlainText;
        private System.Windows.Forms.Button btnEncrypt;
        private System.Windows.Forms.Button btnDecrypt;
        private System.Windows.Forms.TextBox txtCipherText;
        private System.Windows.Forms.TextBox txtKeyEncoded;
        private System.Windows.Forms.TextBox txtIVEncoded;
        private System.Windows.Forms.GroupBox gbxEncrypt;
        private System.Windows.Forms.GroupBox gbxDecrypt;
        private System.Windows.Forms.TextBox txtDecryptedText;
        private System.Windows.Forms.TextBox txtIVEncoded2;
        private System.Windows.Forms.TextBox txtKeyEncoded2;
        private System.Windows.Forms.Button btnReset;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtEncryptedText;
        private System.Windows.Forms.CheckBox chkGenerateKeyAndIV;
    }
}

