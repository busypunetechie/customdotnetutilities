﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Windows.Forms;
using FHLBSF.Crypto;

namespace FHLBSF.EncUtil
{
    public partial class Form1 : Form
    {
        AES _aes = null;

        public Form1()
        {
            InitializeComponent();
            _aes = new AES();
            this.txtKeyEncoded.Text = _aes.KeyEncoded;
            this.txtIVEncoded.Text = _aes.IVEncoded;
        }

        private void chkGenerateKeyAndIV_CheckedChanged(object sender, EventArgs e)
        {
            if (this.txtKeyEncoded.ReadOnly)
            {
                this.btnReset.Enabled = false;
                this.txtIVEncoded.ReadOnly = false;
                this.txtKeyEncoded.ReadOnly = false;
            }
            else
            {
                this.btnReset.Enabled = true;
                this.txtIVEncoded.ReadOnly = true;
                this.txtKeyEncoded.ReadOnly = true;
            }
        }

        private void btnEncrypt_Click(object sender, EventArgs e)
        {
            try
            {
                _aes.KeyEncoded = this.txtKeyEncoded.Text;
                _aes.IVEncoded = this.txtIVEncoded.Text;

                this.txtKeyEncoded2.Text = _aes.KeyEncoded;
                this.txtIVEncoded2.Text = _aes.IVEncoded;
                this.txtEncryptedText.Text = _aes.EncryptAndEncode(this.txtPlainText.Text);
                this.txtCipherText.Text = this.txtEncryptedText.Text;
            }
            catch (Exception ex)
            {
                MessageBox.Show(string.Format("Invalid Key/IV\n\n{0}", ex.Message), "Error");
            }
        }

        private void btnDecrypt_Click(object sender, EventArgs e)
        {
            try
            {
                _aes.KeyEncoded = this.txtKeyEncoded2.Text;
                _aes.IVEncoded = this.txtIVEncoded2.Text;
                this.txtDecryptedText.Text = _aes.DecodeAndDecrypt(this.txtCipherText.Text);
            }
            catch (Exception ex)
            {
                MessageBox.Show(string.Format("Invalid Key/IV\n\n{0}", ex.Message), "Error");
            }
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            _aes = new AES();
            this.txtKeyEncoded.Text = _aes.KeyEncoded;
            this.txtIVEncoded.Text = _aes.IVEncoded;
        }

        internal void DoSomeDecryption()
        {
            string connectionStringEncoded = "DPpcQxJxRbiTfld21ynFinSheW8Gs68VHFXp0WmQofI=";
            string keyEncoded = "M684cTFDxyrAfgb5FbWg5Ilg7QuBstm7DHc8fFDxK6g=";
            string ivEncoded = "YTGP76ZgUhVUREjVAzgOaA==";

            AES aes = new AES();
            aes.KeyEncoded = keyEncoded;
            aes.IVEncoded = ivEncoded;
            string connectionStringPlainText = aes.DecodeAndDecrypt(connectionStringEncoded);
        }

    }
}
